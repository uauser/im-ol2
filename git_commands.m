%% retreive
% iteratieve methode-oefeningen les 1
!git clone https://uauser@bitbucket.org/uauser/im-ol1.git 
% iteratieve methode-oefeningen les 2
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git

%% Om zelf lokaal een git repository op te zetten
!git init
!git add Poisson2D.m
!git commit -m "De Poisson2D.m file toegevoegd."
!git checkout master

