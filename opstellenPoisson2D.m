%% De laplace operator
figure(1)
N = 15;
%
text(0,.5,'$$ \Delta u =  \left[   \begin{tabular}{c} $\frac{  \delta^2  u}{ \delta x^2}$  \\   $\frac{\delta^2 u}{\delta y^2}$ \end{tabular} \right] $$'...
    ,'Interpreter', 'latex','fontsize',25);
axis off
%% Benadering
figure(2)
xy = zeros(15);
imagesc(xy);
xy(7,7)=1;
imagesc(xy);
xy(6,7)=.5;
xy(8,7)=.5;
xy(7,6)=.5;
xy(7,8)=.5;
imagesc(xy);
text(7,7,'$u_{i,j}$','fontsize',35,'Interpreter', 'latex','HorizontalAlignment','center')
text(6,7,'$u_{i-1,j}$','fontsize',35,'Interpreter', 'latex','HorizontalAlignment','center')
text(7,6,'$u_{i,j-1}$','fontsize',35,'Interpreter', 'latex','HorizontalAlignment','center')
%axis off
%% benadering Laplace operator (eq. 1.10)
%figure(2);
title('$$ \frac{-u_{i-1,j} - u_{i,j-1} +4 u_{i,j}- u_{i+1,j} - u_{i,j+1}}{h^2}$$','Interpreter', 'latex','fontsize',25);
axis off;
%% opstellen 1D Poisson probleem p.17
h = 1/(N+1);

e = ones(N,1);
I = speye(N);
D = spdiags( [-e 2*e -e], [-1:1], N, N ) % *1/h^2; % (eq. 1.13)
full(D)
%% 1D => 2D (eq. 1.12)
A = kron(I,D) + kron(D,I);
full(A)
imagesc(full(A))
title('imagesc(full(A))')
%% Voor sparse matrices
spy(A)
title('spy(A)')

%% opstellen cg => p. 32